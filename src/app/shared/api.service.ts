import { Injectable } from '@angular/core';
import * as env from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  apiUrl: string;
  constructor() { 
    this.apiUrl = env.environment['endPoint'];
  }
}
