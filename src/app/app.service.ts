import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiService } from './shared/api.service';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(
    private http: HttpClient,
    private api: ApiService
  ) { }

  getPost() {
    return this.http.get<any>(this.api.apiUrl);
  }
}
