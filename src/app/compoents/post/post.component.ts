import { Component, OnInit } from '@angular/core';
import { AppService} from '../../app.service'

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  postList: Array<any> = []
  constructor(public appService: AppService) { }

  ngOnInit() {
    this.getPost();
  }
  getPost() {
    this.appService.getPost().subscribe(
      res => {
        this.postList = res ? res : [];
        console.log('postList', this.postList)
      }
    );
  }

}
